package com.vss.weatherapp.model

data class Coord(
    val lat: String,
    val lon: String
)