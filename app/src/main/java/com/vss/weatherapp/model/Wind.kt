package com.vss.weatherapp.model

data class Wind(
    val deg: String,
    val speed: String
)