package com.vss.weatherapp.ui

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.vss.weatherapp.databinding.ActivityWeatherBinding
import com.vss.weatherapp.vm.ViewModelFactory
import com.vss.weatherapp.vm.WeatherViewModel
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnSuccessListener
import com.vss.weatherapp.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class WeatherActivity : AppCompatActivity() {
    private lateinit var viewmodel: WeatherViewModel
    private lateinit var binding: ActivityWeatherBinding

    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var lastLocation: Location? = null
    private var locationRequest: LocationRequest? = null
    private var mLocationCallback: LocationCallback? = null
    private val REQUEST_LOCATION_CODE = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWeatherBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewmodel = ViewModelProvider(this, ViewModelFactory()).get(WeatherViewModel::class.java)

        setUpLoacationManager()
        setupObserver()

        binding.refresh.setOnClickListener {
            requestPermissions()
            viewmodel.fetchWeatherForcastData()
        }
    }

    override fun onStart() {
        super.onStart()
        if (!checkPermissions()) {
            startLocationUpdates()
            requestPermissions()
        } else {
            getLastLocation()
            startLocationUpdates()
        }
    }

    override fun onPause() {
        stopLocationUpdates()
        super.onPause()
    }

    private fun requestPermissions() {
        if (!checkGPSEnabled()) {
            return
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                //Location Permission already granted
                startLocationUpdates()
            } else {
                //Request Location Permission
                startLocationPermissionRequest()
            }
        } else {
            startLocationUpdates()
        }
    }

    private fun getAddress(lat: Double?, lag: Double?): String? {
        val geocoder = Geocoder(this, Locale.getDefault())
        var addresses: List<Address>? =
            null // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        try {
            addresses = geocoder.getFromLocation(lat!!, lag!!, 1)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        val value = addresses?.get(0)
        Log.v("FullLocation", value.toString())
        val area = value?.subAdminArea ?: value?.locality
        val address = area+","+value?.countryCode
        return address
    }

    private fun setUpLoacationManager() {
        try {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            mFusedLocationClient?.lastLocation?.addOnSuccessListener(this,
                OnSuccessListener<Location?> { location ->
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        // Logic to handle location object
                        setUpLatLong(location.latitude, location.longitude)
                    }
                })
            locationRequest = LocationRequest.create()
            locationRequest?.interval = 300000
            locationRequest?.fastestInterval = 60000
            locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            mLocationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult) {
                    for (location in locationResult.locations) {
                        // Update UI with location data
                        setUpLatLong(location.latitude, location.longitude)
                    }
                }
            }
        } catch (ex: SecurityException) {
            ex.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setUpLatLong(lat: Double?, long: Double?) {
        val address = getAddress(lat, long)
        viewmodel.add.value =(address)
        viewmodel.fetchWeatherForcastData()
    }

    private fun checkPermissions(): Boolean {
        return (ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED
                )
    }

    private fun startLocationPermissionRequest() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                AlertDialog.Builder(ContextThemeWrapper(this, R.style.AlertDialogTheme))
                    .setTitle("Location Permission Needed")
                    .setMessage("This app needs the Location permission to access the weather activity according to your current location.\nSo please allow the location permission.")
                    .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            REQUEST_LOCATION_CODE
                        )
                    })
                    .create()
                    .show()

            } else ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_LOCATION_CODE
            )
        }
    }


    private fun checkGPSEnabled(): Boolean {
        if (!isLocationEnabled())
            showAlert()
        return isLocationEnabled()
    }

    private fun showAlert() {
        val dialog = AlertDialog.Builder(ContextThemeWrapper(this, R.style.AlertDialogTheme))
        dialog.setTitle("Enable Location")
            .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " + "use this app.\nPurpose location permission only to display weather activity.")
            .setPositiveButton("Location Settings") { paramDialogInterface, paramInt ->
                val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(myIntent)
            }
            .setNegativeButton("Cancel") { paramDialogInterface, paramInt -> }
        dialog.show()
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_LOCATION_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        //Toast.makeText(this, "permission granted", Toast.LENGTH_LONG).show()
                        getLastLocation()
                    }
                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission.
                    // Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show()
                    startLocationPermissionRequest()
                }
                return
            }
        }
    }

    @SuppressWarnings("MissingPermission")
    private fun getLastLocation() {
        mFusedLocationClient?.lastLocation?.addOnCompleteListener(this,
            OnCompleteListener<Location?> { task ->
                if (task.isSuccessful && task.result != null) {
                    lastLocation = task.result
                    setUpLatLong(lastLocation?.latitude, lastLocation?.longitude)
                } else {
                    Log.w("Last Loaction ", "getLastLocation:exception", task.exception)
                    //todo
                    startLocationPermissionRequest()
                }
            })
    }

    private fun stopLocationUpdates() {
        mFusedLocationClient!!.removeLocationUpdates(mLocationCallback)
    }

    private fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        mFusedLocationClient!!.requestLocationUpdates(locationRequest, mLocationCallback, null)
    }

    private fun setupObserver() {
        viewmodel.loader.observe(this,{
          binding.loader.visibility = it
        })

        viewmodel.mainView.observe(this, {
         binding.mainContainer.visibility = it
        })

        viewmodel.errorView.observe(this, {
         binding.errorText.visibility = it
        })

        viewmodel.data.observe(this, {
            val main = it.main
            val sys = it.sys
            val wind = it.wind
            val weather = it.weather[0]
            val updatedAt: Long = it.dt.toLong()
            val updatedAtText =
                "Updated at: " + SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH).format(
                    Date(updatedAt * 1000)
                )
            val temp = main.temp + "°C"
            val tempMin = "Min Temp: " + main.temp_min + "°C"
            val tempMax = "Max Temp: " + main.temp_max + "°C"
            val pressure = main.pressure
            val humidity = main.humidity

            val sunrise: Long = sys.sunrise.toLong()
            val sunset: Long = sys.sunset.toLong()
            val windSpeed = wind.speed
            val weatherDescription = weather.description

            val address = it.name + ", " + sys.country

            binding.address.text = address
            binding.updatedAt.text = updatedAtText
            binding.status.text = weatherDescription.capitalize()
            binding.temp.text = temp
            binding.tempMin.text = tempMin
            binding.tempMax.text = tempMax
            binding.sunrise.text =
                SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(Date(sunrise * 1000))
            binding.sunset.text =
                SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(Date(sunset * 1000))
            binding.wind.text = windSpeed
            binding.pressure.text = pressure
            binding.humidity.text = humidity.toString()
        })
    }
}
