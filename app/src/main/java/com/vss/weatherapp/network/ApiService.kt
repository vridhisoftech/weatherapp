package com.vss.weatherapp.network

import com.vss.weatherapp.model.ResponseApi
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("data/2.5/weather")
    suspend fun getData(
        @Query("q") location: String,
        @Query("units") units: String,
        @Query("appid") api: String
    ): Response<ResponseApi>
}