package com.vss.weatherapp.model

data class Sys(
    val country: String,
    val id: String,
    val sunrise: String,
    val sunset: String,
    val type: String
)