package com.vss.weatherapp.vm

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vss.weatherapp.model.ResponseApi
import com.vss.weatherapp.repo.WeatherRepository
import kotlinx.coroutines.launch
import java.lang.Exception

public class WeatherViewModel(private val repository: WeatherRepository): ViewModel() {

     val add = MutableLiveData<String>()
     private val address: LiveData<String> get() = add

    private val loader_ = MutableLiveData<Int>()
    val loader: LiveData<Int> get() = loader_

    private val mainView_ = MutableLiveData<Int>()
    val mainView: LiveData<Int> get() = mainView_

    private val errorView_ = MutableLiveData<Int>()
    val errorView: LiveData<Int> get() = errorView_

    private val data_ = MutableLiveData<ResponseApi>()
    val data: LiveData<ResponseApi> get() = data_

    fun fetchWeatherForcastData(){
        viewModelScope.launch {
            try {
                setVisibility(errorView_, View.GONE)
                setVisibility(mainView_, View.GONE)
                setVisibility(loader_, View.VISIBLE)
                val data = repository.fetchData(address.value.toString(), "metric")
                setVisibility(loader_, View.GONE)
                data_.postValue(data)
                setVisibility(mainView_, View.VISIBLE)
            }catch (e: Exception){
                setVisibility(loader_, View.GONE)
                setVisibility(mainView_, View.GONE)
                setVisibility(errorView_, View.VISIBLE)
            }
        }
    }

    private fun setVisibility(liveData: MutableLiveData<Int>, visibility: Int) = visibility.also { liveData.postValue(it) }

    override fun onCleared() {
        super.onCleared()
    }
}