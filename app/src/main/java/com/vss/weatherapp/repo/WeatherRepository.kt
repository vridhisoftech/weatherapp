package com.vss.weatherapp.repo

import com.vss.weatherapp.BuildConfig
import com.vss.weatherapp.model.ResponseApi
import com.vss.weatherapp.network.WebCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class WeatherRepository {

    private val apiService by lazy { WebCall.create() }

    suspend fun fetchData(location: String, units: String): ResponseApi = withContext(Dispatchers.IO) {
        apiService.getData(location,units, BuildConfig.Api_Key).let {
            response -> response.body()!!
        }
    }


    companion object {
        fun getInstance(): WeatherRepository{
            val instance : WeatherRepository by lazy { WeatherRepository() }
            return instance
        }
    }
}